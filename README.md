This program helps you open an editorial for SRM round

Note: It doesn't work with old editorials

The story behind this: 
I usually spend a few minutes every time I google editorials for SRM.
It may be because I'm bad at it, or it may be because the website doesn't have the best UI in the world.
One day I decided to write this huge project with super complicated algorithms that helps you find the SRM editorial in 2 seconds from terminal.
Hope this helps!

How to use:

1. Clone the repo
2. If you are using Ubuntu, add "alias mytc = "bash /home/whatever/path/to/main.sh" at the end of the "~/.bashrc"
3. The command "mytc" could be anything, and make sure you put the right path. 
4. Run "source ~/.bashrc". (You only need it when you first use it)
5. Now you are good to go. 
   Any time you want to open editorials for SRM, just simply type on the command line
   "mytc 678" and it'll open the editorial for SRM 678.

