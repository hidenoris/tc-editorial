if [ "$1" != "" ]; then
  echo "Opening the editorial for SRM $1..."
  google-chrome http://apps.topcoder.com/wiki/display/tc/SRM+"$1"
else
  echo "Enter SRM round number"
  read a
  echo "Opening the editorial for SRM $a..."
  google-chrome http://apps.topcoder.com/wiki/display/tc/SRM+"$a"
fi

